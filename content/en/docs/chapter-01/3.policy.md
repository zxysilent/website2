---
title: "Load balance policy"
description: ""
lead: ""
date: 2022-04-12T20:11:31+08:00
lastmod: 2022-04-12T20:11:31+08:00
draft: false
images: []
menu:
  docs:
    parent: "chapter-01"
weight: 13000
toc: true
---

# Load balance policy

We can indicate a special load balance policy when we create engine group. There are 5 load balances on xorm. They are `RandomPolicy`, `WeightRandomPolicy`, `RoundRobinPolicy`, `WeightRoundRobinPolicy` and `LeastConnPolicy`. You can also implement yourself policy according `GroupPolicy` interface.

* RandomPolicy

```Go
import (
    _ "github.com/lib/pq"
    "xorm.io/xorm"
)

var eg *xorm.EngineGroup

func main() {
	conns := []string{
		"postgres://postgres:root@localhost:5432/test?sslmode=disable;",
		"postgres://postgres:root@localhost:5432/test1?sslmode=disable;",
		"postgres://postgres:root@localhost:5432/test2?sslmode=disable",
	}

    var err error
	eg, err = xorm.NewEngineGroup("postgres", conns, xorm.RandomPolicy())
}
```

* WeightRandomPolicy

```Go
import (
    _ "github.com/lib/pq"
    "xorm.io/xorm"
)

var eg *xorm.EngineGroup

func main() {
    conns := []string{
		"postgres://postgres:root@localhost:5432/test?sslmode=disable;",
		"postgres://postgres:root@localhost:5432/test1?sslmode=disable;",
		"postgres://postgres:root@localhost:5432/test2?sslmode=disable",
	}

	var err error
	// set the weights
	eg, err = xorm.NewEngineGroup("postgres", conns, xorm.WeightRandomPolicy([]int{2, 3}))
}
```

* RoundRobinPolicy

```Go
import (
    _ "github.com/lib/pq"
    "xorm.io/xorm"
)

var eg *xorm.EngineGroup

func main() {
    conns := []string{
		"postgres://postgres:root@localhost:5432/test?sslmode=disable;",
		"postgres://postgres:root@localhost:5432/test1?sslmode=disable;",
		"postgres://postgres:root@localhost:5432/test2?sslmode=disable",
	}

    var err error
	eg, err = xorm.NewEngineGroup("postgres", conns, xorm.RoundRobinPolicy())
}
```

* WeightRoundRobinPolicy

```Go
import (
    _ "github.com/lib/pq"
    "xorm.io/xorm"
)

var eg *xorm.EngineGroup

func main() {
    conns := []string{
		"postgres://postgres:root@localhost:5432/test?sslmode=disable;",
		"postgres://postgres:root@localhost:5432/test1?sslmode=disable;",
		"postgres://postgres:root@localhost:5432/test2?sslmode=disable",
	}

    var err error
    // set the weights
	eg, err = xorm.NewEngineGroup("postgres", conns, xorm.WeightRoundRobinPolicy([]int{2, 3}))
}
```

* LeastConnPolicy

```Go
import (
    _ "github.com/lib/pq"
    "xorm.io/xorm"
)

var eg *xorm.EngineGroup

func main() {
    conns := []string{
		"postgres://postgres:root@localhost:5432/test?sslmode=disable;",
		"postgres://postgres:root@localhost:5432/test1?sslmode=disable;",
		"postgres://postgres:root@localhost:5432/test2?sslmode=disable",
	}

    var err error
	eg, err = xorm.NewEngineGroup("postgres", conns, xorm.LeastConnPolicy())
}
```

* Customerize Policy

You can also implement yourself policy according `GroupPolicy` interface.

```Go
type GroupPolicy interface {
	Slave(*EngineGroup) *Engine
}
```
